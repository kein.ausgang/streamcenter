# Streamcenter

Simple streaming server (nginx-rtmp) deployment with ansible.
Tested on Debian 10.

Streaming server with basic security ( htpasswd authentification).
Comment role 'rtmp-auth' if you want it open  bar.

## Dependencies

  1. Install **ansible** on your local computer.  
  `brew install ansible`

  2. Get **ssh access** to remote server with ssh key.  
     ~/.ssh/config is your friend

  3. Install **python3** on the remote server.  
  `ssh root@remote "export http_proxy=http://proxy.com; brew install python3"`


## Deployment

  1. Edit hosts.example file.

  2. Rename hosts.example to hosts.

  3. Start the playbook.  
    `ansible-playbook -i hosts install.yml`

## Usage


  * Create an access token.  
   Go to https://EXAMPLE.COM/auth/create.php  
   Use login and password given in hosts file.

  * Start streaming with the given url / streamkey.

  * Playback with the given url.  
   or go to https://EXAMPLE.COM/PLAY

## Project structure

### Hosts

File containing remote server(s) and their ip(s).  
Variables are placed here.

## Roles

Ansible deployment is split into roles.

**common**

  * Install useful tools (only VIM actually).
  * Disable ssh password, better using ssh key.

**nftables**

  * Install nftables (firewall).
  * Copy nftables config file with custom rules.

**nginx-rtmp**

  * Install nginx, certbot and dependencies.
  * Copy rtmp config file for nginx.
  * Get certificate with certbot.

**rtmp-auth**

 Enable rtmp and web authentification.

   * Enable rtmp auth in rtmp.conf.
   * Add php authentification script.
   * Add php token creation script.
   * Set basic web auth for creation script.
   * Add cron to delete old token.

**webplayer**

Add basic web player pages.

  * Add webplayer to list/play all available streams.
  * Add webplayer to play stream given in url.

## Contact

You can reach me at @kein.ausgang:matrix.org




