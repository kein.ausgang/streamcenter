<!DOCTYPE html>
<html>
<body>

<h1>Tokens list</h1>

<h2>Static streams</h2>

<p>push url : rtmp://EXAMPLE.COM/staticX<br />
playback url : rtmp://EXAMPLE.COM/staticX/playbackkey</p>

<?php

$token_files = glob('*.static*');
foreach($token_files as $token_file) {
    $token = substr ($token_file, 0, -7);
    $content = file_get_contents($token_file);
    echo '<p> streamkey: <b>'.$token.'</b> , playbackkey: <b>'.$content.'</b></p>';
}
?>


<h2>Dynamic streams</h2>

<p>push url : rtmp://EXAMPLE.COM/rtmp_app_name<br />
playback url : rtmp://EXAMPLE.COM/rtmp_app_name/playbackkey</p>

<?php

$token_files = glob('*.token');
foreach($token_files as $token_file) {
    $token = substr ($token_file, 0, -6);
    $content = file_get_contents($token_file);
    echo '<p> streamkey: <b>'.$token.'</b> , playbackkey: <b>'.$content.'</b></p>';
}
?>

</body>
</html>