<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>videojs-contrib-hls embed</title>
<link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
<script src="https://unpkg.com/video.js/dist/video.js"></script>
<script src="https://unpkg.com/@videojs/http-streaming/dist/videojs-http-streaming.js"></script>
</head>


<body onload="playthis()">
  <h1>Play</h1>

  <video id=videoid width=960 height=540 preload="auto" class="video-js vjs-default-skin" controls muted>
  <source
     type="application/x-mpegURL">
</video>

<script>
var player = videojs('videoid');
player.play();

function playthis(){
    const params = new URLSearchParams(window.location.search);
    const stream = params.get("stream");
    var player = videojs('videoid');
    player.src({type: 'application/x-mpegURL', src:'../hls/'+stream+'.m3u8'});
    player.play();
}

</script>
  
</body>
</html>