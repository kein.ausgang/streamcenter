<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>videojs-contrib-hls embed</title>
<link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
<script src="https://unpkg.com/video.js/dist/video.js"></script>
<script src="https://unpkg.com/@videojs/http-streaming/dist/videojs-http-streaming.js"></script>
<script src="streamcenter.js" type="text/javascript"></script>
</head>

<body>
  <h1>StreamCenter</h1>

<div id="videoplayer">
    <video-js id="videoid" class="vjs-default-skin" preload="auto" height="300" controls muted>
    <source id="video_src" src="../hls/toto.m3u8" type="application/x-mpegURL">
    </video-js>
</div>


<div id="buttons">
    
    <?php
    $playlists = glob('../hls/*.{m3u8}', GLOB_BRACE);
    foreach($playlists as $playlist) {
            $streamname = substr ($playlist, 7, -5);
            echo '<button onclick="load_source(\''. $playlist .'\')">' . $streamname. '</button><br/>';
    }
    ?>
</div>


  
</body>
</html>